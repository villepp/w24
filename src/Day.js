import React from "react";

const Day = ({ month }) => {
  const getStartDayOfWeek = (month) => {
    const date = new Date(2021, month - 1, 1);
    const weekdays = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    return weekdays[date.getDay()];
  };

  const startDayOfWeek = getStartDayOfWeek(month);

  return (
    <div>
      Month {month}/2021 starts {startDayOfWeek}
    </div>
  );
};

export default Day;
