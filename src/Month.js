// src/components/Month.js
import React, { useState } from "react";
import Day from "./Day";

const Month = () => {
  const [selectedMonth, setSelectedMonth] = useState("1");

  const months = Array.from({ length: 12 }, (_, i) => i + 1);
  const monthOptions = months.map((monthNumber) => (
    <option key={monthNumber} value={monthNumber}>
      {monthNumber}
    </option>
  ));

  const handleMonthChange = (e) => {
    setSelectedMonth(e.target.value);
  };

  return (
    <div>
      <select name="month" onChange={handleMonthChange} value={selectedMonth}>
        {monthOptions}
      </select>
      <Day month={selectedMonth} />
    </div>
  );
};

export default Month;
